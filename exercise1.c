/** A program written with a non-tail recursive function which 
 *  prints triangle pattern for a certain number
 */
#include<stdio.h>

void pattern(int n);    //function prototype

/** main function
 */
int main()
{
    pattern(4);
    return 0;
}

/** this function takes a number passed from main program
 *  and print a number pattern as a triangle
 *  (non-tail recursive function)
 */
void pattern(int n)
{
    if (n == 0)
        return;
    else
        {
            pattern(n - 1); // recursive call
            for (int i = n; i >= 1; i--)
            {
                printf("%d ", i);
            }
            printf("\n");
        }
}
