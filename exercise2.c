/** A program written with a non-tail recursive function which 
 *  prints fibonacci sequence up to a certain value
 */
#include <stdio.h>

// funtion prototypes
void fibonacciSeq(int n);
int fibonacci(int n); 

/** main function
 * 
 */
int main()
{
    fibonacciSeq(10);

    return 0;
}

/** this function prints the fibonnaci number upto
 *  the number passed by main function
 * (non - tail recursive function)
 */
void fibonacciSeq(int n)
{
    if (n==0)
    {
        return;
    }
    else
    {
        fibonacciSeq(n - 1);        //recursive call
        printf("%d\n",fibonacci(n));
    }
    
}

/** this function determines the fibonacci number 
 *  for a number passed as a parameter
 */
int fibonacci(int n)
{
    if (n == 0)
    {
        return 0;
    }
    else if (n == 1)
        {
            return 1;
        }
    else
    {
        return fibonacci(n - 1) + fibonacci(n - 2);
    }
}

